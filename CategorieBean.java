package tn.esprit;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;

import tn.esprit.PIPFE.Entities.Categorie;
import tn.esprit.PIPFE.Entities.Encadrant;
import tn.esprit.PIPFE.Entities.FichePFE;
import tn.esprit.PIPFE.Entities.PreValidateur;
import tn.esprit.PIPFE.Entities.Rapporteur;
import tn.esprit.PIPFE.Services.CrudServices;

@ManagedBean
@SessionScoped
public class CategorieBean {
	
	private int Id;
	private String Nom;
	private Boolean valider;
	private List<Encadrant> Encadrants;
	private List<PreValidateur> Prevalidateurs;
	private List<Rapporteur> Rapporteurs;
	private FichePFE fichePfe;
	private List<Categorie> Categories;
	private List<FichePFE> fichePfes;
	private static int a;
	@EJB
	CrudServices cs;
	
	@PostConstruct
	public void Init(){
		valider=false;
	}
	
	public List<Categorie> AfficherCat() {
		return Categories= cs.getAllCategorie();
	}
	
public void Validate(int i)
{
	cs.ValiderCat(i);
}
	
	public List<Categorie> AfficherListCategorieEncadrant(int id) {
		return cs.AfficherListCategorieEncadrant(id);
	}
	
	public List<Categorie> AfficherListCategorieRapporteur(int id) {
		return cs.AfficherListCategorieRapporteur(id);
	}

	public List<Categorie> getCategories() {
		return Categories;
	}

	public void setCategories(List<Categorie> categories) {
		Categories = categories;
	}

	public List<FichePFE> getFichePfes() {
		return fichePfes;
	}

	public void setFichePfes(List<FichePFE> fichePfes) {
		this.fichePfes = fichePfes;
	}
	
	public List<FichePFE> getcata()
	{
		a=cs.getnbandroid().size();
	return	fichePfes=cs.getnbandroid();
	}
	public List<FichePFE> getcatb()
	{
	return	fichePfes=cs.getnbbi();
	}
	public List<FichePFE> getcath()
	{
	return	fichePfes=cs.getnbhtml();
	}
	public List<FichePFE> getcati()
	{
	return	fichePfes=cs.getnbios();
	}
	public List<FichePFE> getcatj()
	{
	return	fichePfes=cs.getnbjava();
	}
	public List<FichePFE> getcatje()
	{
	return	fichePfes=cs.getnbjavaee();
	}
	public List<FichePFE> getcatjs()
	{
	return	fichePfes=cs.getnbjs();
	}
	public List<FichePFE> getcatn()
	{
	return	fichePfes=cs.getnbnet();
	}
	public List<FichePFE> getcatp()
	{
	return	fichePfes=cs.getnbpython();
	}
	public List<FichePFE> getcatw()
	{
	return	fichePfes=cs.getnbweb();
	}
	public List<FichePFE> getFichePFEs() {
		return fichePfes=cs.getAllFichePFE();
	}

	public static int getA() {
		return a;
	}

	public static void setA(int a) {
		CategorieBean.a = a;
	}
	
	

	
}
