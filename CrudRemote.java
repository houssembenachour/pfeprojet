package tn.esprit.PIPFE.Services;

import java.util.Date;
import java.util.List;

import javax.ejb.Remote;

import tn.esprit.PIPFE.Entities.Categorie;
import tn.esprit.PIPFE.Entities.Classe;
import tn.esprit.PIPFE.Entities.Departement;
import tn.esprit.PIPFE.Entities.Encadrant;
import tn.esprit.PIPFE.Entities.Enseignant;
import tn.esprit.PIPFE.Entities.Etudiant;
import tn.esprit.PIPFE.Entities.FichePFE;
import tn.esprit.PIPFE.Entities.FormulaireConvention;
import tn.esprit.PIPFE.Entities.Options;
import tn.esprit.PIPFE.Entities.PreValidateur;
import tn.esprit.PIPFE.Entities.President;
import tn.esprit.PIPFE.Entities.Rapporteur;
import tn.esprit.PIPFE.Entities.Site;
import tn.esprit.PIPFE.Entities.User;


@Remote
public interface CrudRemote {
	
	
	void AjouterConvention (FormulaireConvention h);
	FormulaireConvention AfficherConvention (int id);
	void ModifierConvention (FormulaireConvention h);
	List getAllConvention();
	void deleteConvention(FormulaireConvention h);
	
	void AjouterEtudiant (Etudiant h);
	Etudiant AfficherEtudiant (int id);
	void ModifierEtudiant (Etudiant h);
	List getAllEtudiant();
	void deleteEtudiant(Etudiant h);
	
	void AjouterSite (Site h);
	Site AfficherSite (int id);
	void ModifierSite (Site h);
	List getAllSites();
	void deleteSite(Site h);
	
	void AjouterDepartement (Departement h);
	Departement AfficherDepartement (int id);
	void ModifierDepartement (Departement h);
	List getAllDepartement();
	void deleteDepartement(Departement h);
	
	void AjouterClasse (Classe h);
	Classe AfficherClasse (int id);
	void ModifierClasse (Classe h);
	List getAllClasses();
	void deleteClasse(Classe h);
	
	void AjouterOptions (Options p);
	Options AfficherOptions (int id);
	void ModifierOptions (Options h);
	List getAllOptions();
	void deleteOptions(Options h);
	
	void Affecterdepasit(int ids ,int idd);
	void Affecteropadep(int idd ,int ido);
	void Affecterclassaop(int ido ,int idc);
	void Affecteretuaclass(int idc ,int ide);
	
	void AjouterRapporteur(Rapporteur rr);
	void AjouterPreValidateur(PreValidateur P);
	void AjouterEncadrant(Encadrant E);
	void AjouterPresident(President P);
	void AjouterEnseignant(Enseignant E);
	void AffecterCatEncadrant(int idE, int idC);
	void AffecterCatPrevalidateur(int idE, int idC);
	void AffecterEnsAFiche(int idf, int ide);
	void AffecterRoleToRapporteur(int Ide);
	void AffecterRoleToEncadrant(int Ide);
	void AffecterRoleToPreValidateur(int Ide);
	void AffecterRoleToPresident(int Ide);

	
	void AjouterCategorie(Categorie c);
	public Categorie AfficherCategorie(int id);
	public List AfficherCategorie();
	List<Categorie> AfficherListCategorieEncadrant(int id);
	List<Categorie> AfficherListCategorieEEncadrant(int id);
	List<Categorie> AfficherListCategorieRapporteur(int id);
	List<Categorie> AfficherListCategorieFiche(int id);
	
	void AffecterEncadrantFichePFE(int id,int id1);
	void AffecterEcadrantFichePfe(int idf,int ide);

	public void AjouterCategorieToEncadrant(int id,int idc);
	void AjouterCategorieToRapporteur(int id,int idc);
	void removeCategorieEncadrant(int id, int idc);
	void removeCategorieRapporteur(int id, int idc);
	
	void AjouterFichePfe(FichePFE f);
	public void AffecterEtudiantFiche(int idE, int idF);
	List<FichePFE> AfficherListeFiche();
	List<FichePFE> AfficherListeFicheRole(int role, int id);
	
	
	void NoterEncadrant(int idfiche,float note);
	void NoterRapporteur(int idfiche,float note);
	List<FichePFE> getFichesParDate(Date debut,Date fin);
	List<FichePFE> getFichesParIDetDate(int ide, Date debut,Date fin);
	List<FichePFE> getFichesParIDetDateModifier(int ide, Date debut,Date fin);
	List<FichePFE> getFichesParIDetDatePrevalider(int ide, Date debut,Date fin);
	void AccepterModificationMajeure(int idf);
	void Prevalider(int idf);
	
	public User login(String username , String password);
	
	public Encadrant getEncadrantQuery(User u);
	public PreValidateur getPrevalidateurQuery(User u);
	public President getPresidentQuery(User u);
	public Rapporteur getRapporteurQuery(User u);
	
	
	///houssem
	Encadrant getEncadrant(int id);
	Enseignant getEnseignat(int id);
	
	
}
