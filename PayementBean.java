package tn.esprit.ManagedBeans;


import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.SessionScoped;

import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.model.Token;

import tn.esprit.PIPFE.Entities.Payement;
import tn.esprit.PIPFE.Services.CrudServices;


@ManagedBean
@SessionScoped
public class PayementBean {
	
	private int Id;
	private java.util.Date Date;
	private String Description;
	int adminid;
	List<Payement> payements;
	private Boolean payed;
	private float price;
	private static final String TEST_STRIPE_SECRET_KEY = "sk_test_bh5IZWGbVLRt3AgnFrlj4ZQG";
	String card_num;
	String exp_month;
	String exp_year;
	String cvc;
	
	@EJB
	CrudServices cs;
	
	
	@ManagedProperty(value="#{userBean}")
    private UserBean user;
	
	@PostConstruct
	public void Init(){
	Stripe.apiKey = TEST_STRIPE_SECRET_KEY;
	}
	
	
	
	public String getCard_num() {
		return card_num;
	}



	public void setCard_num(String card_num) {
		this.card_num = card_num;
	}



	public String getExp_month() {
		return exp_month;
	}



	public void setExp_month(String exp_month) {
		this.exp_month = exp_month;
	}



	public String getExp_year() {
		return exp_year;
	}



	public void setExp_year(String exp_year) {
		this.exp_year = exp_year;
	}



	public String getCvc() {
		return cvc;
	}



	public void setCvc(String cvc) {
		this.cvc = cvc;
	}



	public Boolean getPayed() {
		return payed;
	}
	public void setPayed(Boolean payed) {
		this.payed = payed;
	}
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public List<Payement> getPayements() {
		payements=cs.getPayements(user.getUser());
		return payements;
	}
	public void setPayements(List<Payement> payements) {
		this.payements = payements;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public java.util.Date getDate() {
		return Date;
	}
	public void setDate(java.util.Date date) {
		Date = date;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public int getAdminid() {
		return adminid;
	}
	public void setAdminid(int adminid) {
		this.adminid = adminid;
	}
	public UserBean getUser() {
		return user;
	}
	public void setUser(UserBean user) {
		this.user = user;
	}
	
	

	   
	    public String createToken() {
	               
	        Map<String, Object> tokenParams = new HashMap<String, Object>();
	                Map<String, Object> cardParams = new HashMap<String, Object>();
	                cardParams.put("number", card_num);
	                cardParams.put("exp_month", exp_month);
	                cardParams.put("exp_year", exp_year);
	                cardParams.put("cvc", cvc);
	                tokenParams.put("card", cardParams);

	        try {
	            return Token.create(tokenParams).getId();
	        } catch (StripeException ex) {
	            ex.getMessage();
	        return null;
	        }
	        }

	    public void chargeCreditCard(String amount,String tok) throws StripeException {
	            
	            Map<String, Object> chargeParams = new HashMap<String, Object>();
	            chargeParams.put("amount", amount);
	            chargeParams.put("currency", "eur");
	            chargeParams.put("description", "Charge for jenny.rosen@example.com");
	            chargeParams.put("source", tok);
	            // ^ obtained with Stripe.js
	            Charge.create(chargeParams);
	                }
	    
	    
	    public void values(int id,float price){
	    	this.Id=id;
	    	this.price=price;
	    }
	    

	    public void payment(){
	    	try {
				chargeCreditCard(price+"",createToken());
			} catch (StripeException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	    	cs.payer(Id);
	    }

}

	


