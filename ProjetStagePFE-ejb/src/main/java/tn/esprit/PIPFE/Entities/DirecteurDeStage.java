package tn.esprit.PIPFE.Entities;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;

//import javax.persistence.GeneratedValue;
import java.io.Serializable;

@Entity
@DiscriminatorValue(value = "DirecteurDeStage")
@NamedQueries({
    @NamedQuery(name = "DirecteurDeStage.findAll", query = "SELECT t FROM DirecteurDeStage t")
})
public class DirecteurDeStage extends Enseignant implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private boolean EnableRate;
	public boolean isEnableRate() {
		return EnableRate;
	}
	public void setEnableRate(boolean enableRate) {
		EnableRate = enableRate;
	}
	public DirecteurDeStage() {
		super();
		// TODO Auto-generated constructor stub
	}

	public DirecteurDeStage(int cin, String nom, String prenom, int tel, String address, String email) {
		super(cin, nom, prenom, tel, address, email);
		// TODO Auto-generated constructor stub
	}
	@Override
	public String toString() {
		return "DirecteurDeStage [EnableRate=" + EnableRate + "]";
	}
	
	
	
	

}
