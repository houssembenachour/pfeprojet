package tn.esprit.PIPFE.Entities;

import java.io.Serializable;
import java.util.List;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;

@Entity
@DiscriminatorValue(value = "Encadrant")
@NamedQueries({
    @NamedQuery(name = "Encadrant.findAll", query = "SELECT NEW tn.esprit.PIPFE.Entities.Encadrant (t.Id,t.Cin,t.Nom,t.Prenom,t.Tel,t.Email,t.nmaxEncadrer,t.nencadrer) FROM Encadrant t")
})
public class Encadrant extends Enseignant implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToMany
	private List<Categorie> PrefEncadrerrList;
	
	private int nmaxEncadrer;
	private int nencadrer;
	
	
	public List<Categorie> getPrefEncadrerrList() {
		return PrefEncadrerrList;
	}

	public void setPrefEncadrerrList(List<Categorie> prefEncadrerrList) {
		PrefEncadrerrList = prefEncadrerrList;
	}

	public int getNmaxEncadrer() {
		return nmaxEncadrer;
	}

	public void setNmaxEncadrer(int nmaxEncadrer) {
		this.nmaxEncadrer = nmaxEncadrer;
	}

	public int getnEncadrer() {
		return nencadrer;
	}

	public void setnEncadrer(int nEncadrer) {
		this.nencadrer = nEncadrer;
	}

	public Encadrant() {
		super();
		// TODO Auto-generated constructor stub
	}

	public Encadrant(int nmaxEncadrer) {
		super();
		// TODO Auto-generated constructor stub
		this.nmaxEncadrer= nmaxEncadrer;
		nencadrer=0;
	}

	public Encadrant(int cin, String nom, String prenom, int tel, String address, String email) {
		super(cin, nom, prenom, tel, address, email);
		// TODO Auto-generated constructor stub
	}
	public Encadrant(int id,int cin, String nom, String prenom, int tel, String address, String email) {
		super(id,cin, nom, prenom, tel, address, email);
		// TODO Auto-generated constructor stub
	}

	public Encadrant(String nom, String type_employe, int id) {
		super(nom, type_employe, id);
		// TODO Auto-generated constructor stub
	}
	public Encadrant(String nom, int nmaxEncadrer,int nencadrer) {
		super(nom);
		this.nmaxEncadrer=nmaxEncadrer;
		this.nencadrer=nencadrer;

		// TODO Auto-generated constructor stub
	}

	@Override
	public String toString() {
		
		String s="Encadrant [PrefEncadrerrList=" + PrefEncadrerrList + ", nmaxEncadrer=" + nmaxEncadrer + ", nEncadrer="
				+ nencadrer + ", type_employe=" + type_employe +"]";
		s=s+super.toString();
		return s;
	}
	public int getNencadrer() {
		return nencadrer;
	}

	public void setNencadrer(int nencadrer) {
		this.nencadrer = nencadrer;
	}

	public Encadrant(int id,int cin,String nom,String prenom,int tel,String email,int nmaxencadrer, int nencadrer) {
		super(id,cin,nom,prenom,tel,email);
		this.nmaxEncadrer = nmaxencadrer;
		this.nencadrer = nencadrer;
		
	}
	
	
}
