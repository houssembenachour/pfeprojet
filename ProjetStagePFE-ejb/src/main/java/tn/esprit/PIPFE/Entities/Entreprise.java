package tn.esprit.PIPFE.Entities;

import java.io.Serializable;

import java.util.List;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
//import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;



@Entity
@Table(name = "Entreprise")
@NamedQueries({
    @NamedQuery(name = "Entreprise.findAll", query = "SELECT t FROM Entreprise t")
})

//SELECT NEW  tn.esprit.PIPFE.Entities.Entreprise(t.Nom,t.SiteWeb)
public class Entreprise implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private int Id;
	private String Nom;
	private String SiteWeb;
	private String Adresse;
	private String Pays;
	private String NomResponsable;
	private String PrenomResponsable;
	private String EmailResponsable;
	private int Tel;
	private float score;
	private int nbrrate;
	private float note;
	
	
	
	
	@OneToMany(mappedBy="Entreprise",fetch =FetchType.EAGER)
	private List<FormulaireConvention> FormulaireConvention;
	
	@OneToMany(mappedBy="Entreprise",fetch =FetchType.EAGER)
	private List<EncadrantEntreprise> EncadrantEntreprise;
	
	@OneToMany(mappedBy="Entreprise",fetch =FetchType.EAGER)
	private List<FichePFE> FichePFE;
	
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getSiteWeb() {
		return SiteWeb;
	}
	public void setSiteWeb(String siteWeb) {
		SiteWeb = siteWeb;
	}
	public String getAdresse() {
		return Adresse;
	}
	public void setAdresse(String adresse) {
		Adresse = adresse;
	}
	public String getPays() {
		return Pays;
	}
	public void setPays(String pays) {
		Pays = pays;
	}
	public String getNomResponsable() {
		return NomResponsable;
	}
	public void setNomResponsable(String nomResponsable) {
		NomResponsable = nomResponsable;
	}
	public String getPrenomResponsable() {
		return PrenomResponsable;
	}
	public void setPrenomResponsable(String prenomResponsable) {
		PrenomResponsable = prenomResponsable;
	}
	public String getEmailResponsable() {
		return EmailResponsable;
	}
	public void setEmailResponsable(String emailResponsable) {
		EmailResponsable = emailResponsable;
	}
	public int getTel() {
		return Tel;
	}
	public void setTel(int tel) {
		Tel = tel;
	}


	public List<FormulaireConvention> getFormulaireConvention() {
		return FormulaireConvention;
	}
	public void setFormulaireConvention(List<FormulaireConvention> formulaireConvention) {
		FormulaireConvention = formulaireConvention;
	}
	public List<EncadrantEntreprise> getEncadrantEntreprise() {
		return EncadrantEntreprise;
	}
	public void setEncadrantEntreprise(List<EncadrantEntreprise> encadrantEntreprise) {
		EncadrantEntreprise = encadrantEntreprise;
	}
	public List<FichePFE> getFichePFE() {
		return FichePFE;
	}
	public void setFichePFE(List<FichePFE> fichePFE) {
		FichePFE = fichePFE;
	}
	public Entreprise(String nom, String siteWeb, String adresse, String pays, String nomResponsable,
			String prenomResponsable, String emailResponsable, int tel) {
		super();
		Nom = nom;
		SiteWeb = siteWeb;
		Adresse = adresse;
		Pays = pays;
		NomResponsable = nomResponsable;
		PrenomResponsable = prenomResponsable;
		EmailResponsable = emailResponsable;
		Tel = tel;
	}
	public Entreprise() {
		super();
	}
	
	
	
	
	@Override
	public String toString() {
		return "Entreprise [Id=" + Id + "]";
	}
	public Entreprise(String nom, String siteWeb) {
		super();
		Nom = nom;
		SiteWeb = siteWeb;
	}
	public float getScore() {
		return score;
	}
	public void setScore(float score) {
		this.score = score;
	}
	public int getNbrrate() {
		return nbrrate;
	}
	public void setNbrrate(int nbrrate) {
		this.nbrrate = nbrrate;
	}
	public float getNote() {
		return note;
	}
	public void setNote(float note) {
		this.note = note;
	}
	public void incNbrrate() {
		this.nbrrate=(this.nbrrate)+1;
	}
	
	public Entreprise(int id, String nom, String siteWeb, String adresse, String pays, String nomResponsable,
			String prenomResponsable, String emailResponsable, int tel, float score, int nbrrate, float note,
			List<tn.esprit.PIPFE.Entities.FormulaireConvention> formulaireConvention,
			List<tn.esprit.PIPFE.Entities.EncadrantEntreprise> encadrantEntreprise,
			List<tn.esprit.PIPFE.Entities.FichePFE> fichePFE) {
		super();
		Id = id;
		Nom = nom;
		SiteWeb = siteWeb;
		Adresse = adresse;
		Pays = pays;
		NomResponsable = nomResponsable;
		PrenomResponsable = prenomResponsable;
		EmailResponsable = emailResponsable;
		Tel = tel;
		this.score = score;
		this.nbrrate = nbrrate;
		this.note = note;
		FormulaireConvention = formulaireConvention;
		EncadrantEntreprise = encadrantEntreprise;
		FichePFE = fichePFE;
	}
	
	
	
	

	
	
	
	
	
	
	
	
	
	

}
