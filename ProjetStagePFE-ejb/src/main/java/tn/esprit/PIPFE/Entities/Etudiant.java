package tn.esprit.PIPFE.Entities;
import java.io.Serializable;

import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
@Entity
@Table(name = "Etudiant")
@NamedQueries({
	    @NamedQuery(name = "Etudiant.findAll", query = "SELECT t FROM Etudiant t"),
	
    //@NamedQuery(name = "Etudiant.findAll", query = "SELECT NEW tn.esprit.PIPFE.Entities.Etudiant (t.Id,t.Cin,t.Nom,t.Prenom,t.email,t.tel,t.Autorise,t.depot) FROM Etudiant t"),
    @NamedQuery(name = "Etudiant.findAllDepot", query = "SELECT NEW tn.esprit.PIPFE.Entities.Etudiant (t.Id,t.Cin,t.Nom,t.Prenom,t.email,t.tel,t.Autorise,t.depot) FROM Etudiant t WHERE t.Autorise=true and t.depot=true"),
    @NamedQuery(name = "Etudiant.findAllNoDepot", query = "SELECT NEW tn.esprit.PIPFE.Entities.Etudiant (t.Id,t.Cin,t.Nom,t.Prenom,t.email,t.tel,t.Autorise,t.depot) FROM Etudiant t WHERE t.Autorise=true and t.depot=false")
})
public class Etudiant implements Serializable {
	

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	

	
	@Id
	@GeneratedValue
	private int Id;
	private int Cin;
	private String Nom;
	private String Prenom;
	private String email;
	private String password;
	private int tel;
	private Boolean Autorise;
	@OneToOne
	private FichePFE FichePfe;
	@ManyToOne
	private Classe classe;
	private int demande_enreg;
	private int demade_annul;
	private String demande_text;
	private String champ_annul;
	private Boolean depot;
	private boolean rated ;
	

	
	
	
	
	
	public boolean isRated() {
		return rated;
	}
	public void setRated(boolean rated) {
		this.rated = rated;
	}
	public int getCin() {
		return Cin;
	}
	public void setCin(int cin) {
		Cin = cin;
	}
	public Boolean getDepot() {
		return depot;
	}
	public int getDemande_enreg() {
		return demande_enreg;
	}
	public void setDemande_enreg(int demande_enreg) {
		this.demande_enreg = demande_enreg;
	}
	public int getDemade_annul() {
		return demade_annul;
	}
	public void setDemade_annul(int demade_annul) {
		this.demade_annul = demade_annul;
	}
	public String getDemande_text() {
		return demande_text;
	}
	public void setDemande_text(String demande_text) {
		this.demande_text = demande_text;
	}
	public String getChamp_annul() {
		return champ_annul;
	}
	public void setChamp_annul(String champ_annul) {
		this.champ_annul = champ_annul;
	}
	public Boolean isDepot() {
		return depot;
	}
	public void setDepot(Boolean depot) {
		this.depot = depot;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getNom() {
		return Nom;
	}
	public void setNom(String nom) {
		Nom = nom;
	}
	public String getPrenom() {
		return Prenom;
	}
	public void setPrenom(String prenom) {
		Prenom = prenom;
	}
	public Boolean getAutorise() {
		return Autorise;
	}
	public void setAutorise(Boolean autorise) {
		Autorise = autorise;
	}

	public FichePFE getFichePfe() {
		return FichePfe;
	}
	public void setFichePfe(FichePFE fichePfe) {
		FichePfe = fichePfe;
	}

	public Classe getClasse() {
		return classe;
	}
	public void setClasse(Classe classe) {
		this.classe = classe;
	}

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public int getTel() {
		return tel;
	}
	public void setTel(int tel) {
		this.tel = tel;
	}
	
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	
	
	public Etudiant() {
		super();
	}
	public Etudiant(String nom, String prenom, String email, int tel, Boolean autorise) {
		super();
		Nom = nom;
		Prenom = prenom;
		this.email = email;
		this.tel = tel;
		Autorise = autorise;
	}
	public Etudiant(String nom, String prenom, String email, String password, int tel, Boolean autorise,
			FichePFE fichePfe, Classe classe) {
		super();
		Nom = nom;
		Prenom = prenom;
		this.email = email;
		this.password = password;
		this.tel = tel;
		Autorise = autorise;
		FichePfe = fichePfe;
		this.classe = classe;
	}
	
	
	@Override
	public String toString() {
		return "Etudiant [Id=" + Id + ", Cin=" + Cin + ", Nom=" + Nom + ", Prenom=" + Prenom + ", email=" + email
				+ ", password=" + password + ", tel=" + tel + ", Autorise=" + Autorise 
				+ ", classe=" + classe + ", demande_enreg=" + demande_enreg + ", demade_annul=" + demade_annul
				+ ", demande_text=" + demande_text + ", champ_annul=" + champ_annul + ", depot=" + depot + ", rated="
				+ rated + "]";
	}
	public Etudiant(int id, String nom, String prenom, String email, String password, int tel, Boolean autorise,
			FichePFE fichePfe, Classe classe, int demande_enreg, int demade_annul, String demande_text,
			String champ_annul, Boolean depot) {
		super();
		Id = id;
		Nom = nom;
		Prenom = prenom;
		this.email = email;
		this.password = password;
		this.tel = tel;
		Autorise = autorise;
		FichePfe = fichePfe;
		this.classe = classe;
		this.demande_enreg = demande_enreg;
		this.demade_annul = demade_annul;
		this.demande_text = demande_text;
		this.champ_annul = champ_annul;
		this.depot = depot;
	}
	public Etudiant(int id, int cin, String nom, String prenom, String email, int tel, Boolean autorise,
			Boolean depot) {
		super();
		Id = id;
		Cin = cin;
		Nom = nom;
		Prenom = prenom;
		this.email = email;
		this.tel = tel;
		Autorise = autorise;
		this.depot = depot;
	}
	
	
	public Etudiant(int id, int cin, String nom, String prenom, String email, int tel) {
		super();
		Id = id;
		Cin = cin;
		Nom = nom;
		Prenom = prenom;
		this.email = email;
		this.tel = tel;
	}
	public Etudiant(int id, int cin, String nom, String prenom, String email, String password, int tel,
			Boolean autorise, FichePFE fichePfe, Classe classe, int demande_enreg, int demade_annul,
			String demande_text, String champ_annul, Boolean depot, boolean rated) {
		super();
		Id = id;
		Cin = cin;
		Nom = nom;
		Prenom = prenom;
		this.email = email;
		this.password = password;
		this.tel = tel;
		Autorise = autorise;
		FichePfe = fichePfe;
		this.classe = classe;
		this.demande_enreg = demande_enreg;
		this.demade_annul = demade_annul;
		this.demande_text = demande_text;
		this.champ_annul = champ_annul;
		this.depot = depot;
		this.rated = rated;
	}
	
	

	
	
}
