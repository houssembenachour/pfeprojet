package tn.esprit.PIPFE.Entities;
import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "FichePFE")
@NamedQueries({
    @NamedQuery(name = "FichePFE.findAll", query = "SELECT NEW tn.esprit.PIPFE.Entities.FichePFE (t.Id,t.Titre,t.Description,t.Problematique,t.Fonctionnalite,t.MotsCle,t.Date,t.NoteRapporteur,t.NoteEncadrant,t.Prevalider,t.Accepte,t.depot)FROM FichePFE t"),
    @NamedQuery(name = "FichePFE.findNoNote", query = "SELECT NEW tn.esprit.PIPFE.Entities.FichePFE (t.Id,t.Titre,t.Description,t.Problematique,t.Fonctionnalite,t.MotsCle,t.Date,t.NoteRapporteur,t.NoteEncadrant,t.Prevalider,t.Accepte,t.depot)FROM FichePFE t WHERE t.Prevalider=true and t.Accepte = 'Accepte' "),
   
})
public class FichePFE  implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private int Id;
	private String Titre;
	private int date_year;
	private String Description;
	private String Problematique;
	private String Fonctionnalite;
	private String MotsCle;
	@OneToOne(mappedBy="FichePfe")
	private Etudiant Etudiant;
	@Temporal(TemporalType.DATE)
	private Date Date;
	private float NoteRapporteur;
	private float NoteEncadrant;
	private Boolean Prevalider;
	@ManyToMany(mappedBy="FichePFE",fetch =FetchType.EAGER)
	private List<Enseignant>Enseignant;
	private String Accepte;
	@OneToOne(mappedBy="FichePfe")
	private Soutenance Soutenannce;
	@ManyToOne
	private Entreprise Entreprise;
	
	@OneToMany(mappedBy="fichePfe")
	private List<Categorie> Categories;
	private boolean depot ;
	
	
	public FichePFE(int id, String titre, String description, String problematique, String fonctionnalite,
			String motsCle,Date date, float noteRapporteur, float noteEncadrant, Boolean prevalider, String accepte,
			boolean depot) {
		super();
		Id = id;
		Titre = titre;
		Description = description;
		Problematique = problematique;
		Fonctionnalite = fonctionnalite;
		MotsCle = motsCle;
		NoteRapporteur = noteRapporteur;
		NoteEncadrant = noteEncadrant;
		Prevalider = prevalider;
		Accepte = accepte;
		this.depot = depot;
		Date=date;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getTitre() {
		return Titre;
	}
	public void setTitre(String titre) {
		Titre = titre;
	}
	public int getDate_year() {
		return date_year;
	}
	public void setDate_year(int date_year) {
		this.date_year = date_year;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getProblematique() {
		return Problematique;
	}
	public void setProblematique(String problematique) {
		Problematique = problematique;
	}
	public String getFonctionnalite() {
		return Fonctionnalite;
	}
	public void setFonctionnalite(String fonctionnalite) {
		Fonctionnalite = fonctionnalite;
	}
	public String getMotsCle() {
		return MotsCle;
	}
	public void setMotsCle(String motsCle) {
		MotsCle = motsCle;
	}
	public Etudiant getEtudiant() {
		return Etudiant;
	}
	public void setEtudiant(Etudiant etudiant) {
		Etudiant = etudiant;
	}
	public Date getDate() {
		return Date;
	}
	public void setDate(Date date) {
		Date = date;
	}
	public float getNoteRapporteur() {
		return NoteRapporteur;
	}
	public void setNoteRapporteur(float noteRapporteur) {
		NoteRapporteur = noteRapporteur;
	}
	public float getNoteEncadrant() {
		return NoteEncadrant;
	}
	public void setNoteEncadrant(float noteEncadrant) {
		NoteEncadrant = noteEncadrant;
	}
	public Boolean getPrevalider() {
		return Prevalider;
	}
	public void setPrevalider(Boolean prevalider) {
		Prevalider = prevalider;
	}
	public List<Enseignant> getEnseignant() {
		return Enseignant;
	}
	public void setEnseignant(List<Enseignant> enseignant) {
		Enseignant = enseignant;
	}
	public String getAccepte() {
		return Accepte;
	}
	public void setAccepte(String accepte) {
		Accepte = accepte;
	}
	public Soutenance getSoutenannce() {
		return Soutenannce;
	}
	public void setSoutenannce(Soutenance soutenannce) {
		Soutenannce = soutenannce;
	}
	public Entreprise getEntreprise() {
		return Entreprise;
	}
	public void setEntreprise(Entreprise entreprise) {
		Entreprise = entreprise;
	}
	public List<Categorie> getCategories() {
		return Categories;
	}
	public void setCategories(List<Categorie> categories) {
		Categories = categories;
	}
	public boolean isDepot() {
		return depot;
	}
	public void setDepot(boolean depot) {
		this.depot = depot;
	}
	public FichePFE(int id, String titre, int date_year, String description, String problematique,
			String fonctionnalite, String motsCle, tn.esprit.PIPFE.Entities.Etudiant etudiant, java.util.Date date,
			float noteRapporteur, float noteEncadrant, Boolean prevalider,
			List<tn.esprit.PIPFE.Entities.Enseignant> enseignant, String accepte, Soutenance soutenannce,
			tn.esprit.PIPFE.Entities.Entreprise entreprise, List<Categorie> categories, boolean depot) {
		super();
		Id = id;
		Titre = titre;
		this.date_year = date_year;
		Description = description;
		Problematique = problematique;
		Fonctionnalite = fonctionnalite;
		MotsCle = motsCle;
		Etudiant = etudiant;
		Date = date;
		NoteRapporteur = noteRapporteur;
		NoteEncadrant = noteEncadrant;
		Prevalider = prevalider;
		Enseignant = enseignant;
		Accepte = accepte;
		Soutenannce = soutenannce;
		Entreprise = entreprise;
		Categories = categories;
		this.depot = depot;
	}
	public FichePFE() {
		super();
	}
	@Override
	public String toString() {
		return "FichePFE [Id=" + Id + ", Entreprise=" + Entreprise + "]";
	}
	
	
	
	
	
	
	
	
	
	

}
