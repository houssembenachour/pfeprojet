package tn.esprit.PIPFE.Entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;



@Entity
@Table(name = "Formation")
@NamedQueries({
    @NamedQuery(name = "Formation.findAll", query = "SELECT t FROM Formation t")
})
public class Formation implements Serializable{
	
	@javax.persistence.Id
	@GeneratedValue
	private int Id;
	private String Lieu;
	private String Description;
	private String Cat;
	private Date Date;
	public Formation() {
		super();
	}
	public Formation(int id, String lieu, String description, String cat, java.util.Date date) {
		super();
		Id = id;
		Lieu = lieu;
		Description = description;
		Cat = cat;
		Date = date;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public String getLieu() {
		return Lieu;
	}
	public void setLieu(String lieu) {
		Lieu = lieu;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public String getCat() {
		return Cat;
	}
	public void setCat(String cat) {
		Cat = cat;
	}
	public Date getDate() {
		return Date;
	}
	public void setDate(Date date) {
		Date = date;
	}
	@Override
	public String toString() {
		return "Formation [Id=" + Id + ", Lieu=" + Lieu + ", Description=" + Description + ", Cat=" + Cat + ", Date="
				+ Date + "]";
	}
	
	
	

}
