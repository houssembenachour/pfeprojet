package tn.esprit.PIPFE.Entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;

@Entity
@Table(name = "Inscription")
@NamedQueries({
    @NamedQuery(name = "Inscription.findAll", query = "SELECT t FROM Inscription t")
})
public class Inscription implements Serializable{
	
	@Id
	@GeneratedValue
	private int Id;
	private int Id_Form;
	private int Id_Etu;
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public int getId_Form() {
		return Id_Form;
	}
	public void setId_Form(int id_Form) {
		Id_Form = id_Form;
	}
	public int getId_Etu() {
		return Id_Etu;
	}
	public void setId_Etu(int id_Etu) {
		Id_Etu = id_Etu;
	}
	public Inscription(int id, int id_Form, int id_Etu) {
		super();
		Id = id;
		Id_Form = id_Form;
		Id_Etu = id_Etu;
	}
	public Inscription() {
		super();
	}
	@Override
	public String toString() {
		return "Inscription [Id=" + Id + ", Id_Form=" + Id_Form + ", Id_Etu=" + Id_Etu + "]";
	}
	public Inscription(int id_Form, int id_Etu) {
		super();
		Id_Form = id_Form;
		Id_Etu = id_Etu;
	}
	
	

}
