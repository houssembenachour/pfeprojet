package tn.esprit.PIPFE.Entities;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
@NamedQueries({
    @NamedQuery(name = "Payement.findAll", query = "SELECT t FROM Payement t"),  
})
public class Payement implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue
	private int Id;
	@Temporal(TemporalType.DATE)
	private java.util.Date Date;
	
	private String Description;
	int adminid;
	private Boolean payed;
	private float price;
	
	
	public float getPrice() {
		return price;
	}
	public void setPrice(float price) {
		this.price = price;
	}
	public Boolean getPayed() {
		return payed;
	}
	public void setPayed(Boolean payed) {
		this.payed = payed;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
	public java.util.Date getDate() {
		return Date;
	}
	public void setDate(java.util.Date date) {
		Date = date;
	}
	public String getDescription() {
		return Description;
	}
	public void setDescription(String description) {
		Description = description;
	}
	public int getAdminid() {
		return adminid;
	}
	public void setAdminid(int adminid) {
		this.adminid = adminid;
	}
	public Payement(int id, java.util.Date date, String description, int adminid) {
		super();
		Id = id;
		Date = date;
		Description = description;
		this.adminid = adminid;
	}
	public Payement(java.util.Date date, String description, int adminid) {
		super();
		Date = date;
		Description = description;
		this.adminid = adminid;
	}
	public Payement() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	
	

}
