package tn.esprit.PIPFE.Services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Remote;

import tn.esprit.PIPFE.Entities.Categorie;
import tn.esprit.PIPFE.Entities.Classe;
import tn.esprit.PIPFE.Entities.Departement;
import tn.esprit.PIPFE.Entities.DirecteurDeStage;
import tn.esprit.PIPFE.Entities.Encadrant;
import tn.esprit.PIPFE.Entities.Enseignant;
import tn.esprit.PIPFE.Entities.Entreprise;
import tn.esprit.PIPFE.Entities.Etudiant;

import tn.esprit.PIPFE.Entities.FichePFE;
import tn.esprit.PIPFE.Entities.FormulaireConvention;
import tn.esprit.PIPFE.Entities.Options;
import tn.esprit.PIPFE.Entities.PreValidateur;
import tn.esprit.PIPFE.Entities.President;
import tn.esprit.PIPFE.Entities.Rapporteur;
import tn.esprit.PIPFE.Entities.Site;
import tn.esprit.PIPFE.Entities.Soutenance;


@Remote
public interface CrudRemote {
	
	
	void AjouterConvention (FormulaireConvention h);
	FormulaireConvention AfficherConvention (int id);
	void ModifierConvention (FormulaireConvention h);
	List getAllConvention();
	void deleteConvention(FormulaireConvention h);
	
	void AjouterEtudiant (Etudiant h);
	Etudiant AfficherEtudiant (int id);
	void ModifierEtudiant (Etudiant h);
	List getAllEtudiant();
	void deleteEtudiant(Etudiant h);
	
	void AjouterSite (Site h);
	Site AfficherSite (int id);
	void ModifierSite (Site h);
	List getAllSites();
	void deleteSite(Site h);
	
	void AjouterDepartement (Departement h);
	Departement AfficherDepartement (int id);
	void ModifierDepartement (Departement h);
	List getAllDepartement();
	void deleteDepartement(Departement h);
	
	void AjouterClasse (Classe h);
	Classe AfficherClasse (int id);
	void ModifierClasse (Classe h);
	List getAllClasses();
	void deleteClasse(Classe h);
	
	void AjouterOptions (Options p);
	Options AfficherOptions (int id);
	void ModifierOptions (Options h);
	List getAllOptions();
	void deleteOptions(Options h);
	
	void Affecterdepasit(int ids ,int idd);
	void Affecteropadep(int idd ,int ido);
	void Affecterclassaop(int ido ,int idc);
	void Affecteretuaclass(int idc ,int ide);
	
	void AjouterRapporteur(Rapporteur rr);
	void AjouterPreValidateur(PreValidateur P);
	void AjouterEncadrant(Encadrant E);
	void AjouterPresident(President P);
	void AjouterEnseignant(Enseignant E);
	void AffecterCatEncadrant(int idE, int idC);
	void AffecterCatPrevalidateur(int idE, int idC);
	void AffecterEnsAFiche(int idf, int ide);
	void AffecterRoleToRapporteur(int Ide);
	void AffecterRoleToEncadrant(int Ide);
	void AffecterRoleToPreValidateur(int Ide);
	void AffecterRoleToPresident(int Ide);
	
	void AjouterCategorie(Categorie c);
	
	void AjouterFichePfe(FichePFE f);
	public void AffecterEtudiantFiche(int idE, int idF);
	void FixNEncadrant(int eid, int nR);
	
	void ModifierCategorie(int idc, String nom);
	void DeleteCategorie(int idc);
	Categorie AfficherCategorie(int id);
	void FixNRapporteur(int rid, int nR);
	void FixNPrevalider(int rid, int nR);
	void FixNSoutener(int rid, int nR);
	List getAllEnseignant();
	List getAllEncadrant();
	List getAllPresident();
	List getAllPrevalidateur();
	List getAllRapporteur();
	List getAllRapporteurBy(String txt);
	List getAllEncadrantBy(String txt);
	List getAllPresidentBy(String txt);
	List getAllPrevalidateurBy(String txt);
	List getAllFiche();
	List getAllFicheSansNote();
	List getAllEtudiantNoDepot();
	List getAllEtudiantDepot();
	void AffecterPresiSout(int p,int sout);
	Soutenance getSoutenanceById(int P);
	void ValiderDepot(int id);
	List getAllEtudiantNoDepotBy(String title);
	List<Soutenance> getAllSoutNoPres();
	boolean Estdispo(int idp);
	void RappelResp(int idf,String titre);
	List<Entreprise> getAllEntreprise();

	void AjouterEntreprise(Entreprise e);

	void Rating(int idd);
	Etudiant getEtudiantById(int id);
	DirecteurDeStage getDirecteur(int idd);
	Entreprise getEntrepriseById(int id);


	
	

}
