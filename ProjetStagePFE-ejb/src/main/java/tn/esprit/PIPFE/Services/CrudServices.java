package tn.esprit.PIPFE.Services;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;


import tn.esprit.PIPFE.Entities.Categorie;
import tn.esprit.PIPFE.Entities.Classe;
import tn.esprit.PIPFE.Entities.Departement;
import tn.esprit.PIPFE.Entities.DirecteurDeStage;
import tn.esprit.PIPFE.Entities.Encadrant;
import tn.esprit.PIPFE.Entities.Enseignant;
import tn.esprit.PIPFE.Entities.Entreprise;
import tn.esprit.PIPFE.Entities.Etudiant;

import tn.esprit.PIPFE.Entities.FichePFE;
import tn.esprit.PIPFE.Entities.FormulaireConvention;
import tn.esprit.PIPFE.Entities.Options;
import tn.esprit.PIPFE.Entities.PreValidateur;
import tn.esprit.PIPFE.Entities.President;
import tn.esprit.PIPFE.Entities.Rapporteur;
import tn.esprit.PIPFE.Entities.Site;
import tn.esprit.PIPFE.Entities.Soutenance;
import tn.esprit.PIPFE.Entities.User;



@Stateless
@LocalBean
public class CrudServices implements CrudRemote{

	@PersistenceContext(unitName="ProjetStagePFE-ejb")
	EntityManager em;
	
	
	
	public User login(String username, String password) {
		ArrayList<User> liste = (ArrayList<User> )(em.createNamedQuery("User.findAll",User.class).getResultList());
		for(User u : liste) { 
			if((u.getUsername().matches(username)) && (u.getPassword().matches(password)))
				return u;
		}
		return null;
	}
/* -----------------------------------  Formulaire de Convention -------------------------------------------------- */ 
	public void AjouterConvention(FormulaireConvention h) {
		em.persist(h);
		
	}
	@Override
	public FormulaireConvention AfficherConvention(int id) {
		FormulaireConvention f= em.find(FormulaireConvention.class,id);
		return (f);
	}
	@Override
	public void ModifierConvention(FormulaireConvention h) {
		 em.merge(h);
		
	}
	@Override
	public List<FormulaireConvention> getAllConvention() {
        return em.createNamedQuery("FormulaireConvention.findAll", FormulaireConvention.class).getResultList();

	}
	@Override
	public void deleteConvention(FormulaireConvention h) {
		if (!em.contains(h)) {
            h = em.merge(h);
        }

        em.remove(h);	
	}
	
	
	
	/* ----------------------------------- Etudiant -------------------------------------------------- */ 

	@Override
	public void AjouterEtudiant(Etudiant h) {
		em.persist(h);		
	}
	@Override
	public Etudiant getEtudiantById(int id) {
		Etudiant f= em.find(Etudiant.class,id);
		return f;
	}
	
	
	public void switchRateEtudiant(int id) {
		Etudiant f= em.find(Etudiant.class,id);
		if(f.isRated())
			f.setRated(false);
		else
			f.setRated(true);
	}
	@Override
	public List getAllEtudiantNoDepotBy(String title) {
		String jpql = "SELECT NEW tn.esprit.PIPFE.Entities.Etudiant (t.Id,t.Cin,t.Nom,t.Prenom,t.tel,t.email) FROM Etudiant t WHERE t.Nom  like :param or t.Prenom  like :param ";
		javax.persistence.Query query =em.createQuery(jpql);
		query.setParameter("param", "%"+title+"%");
		return query.getResultList();
		
		
	}
	@Override
	public void ValiderDepot(int id) {
		Etudiant f= em.find(Etudiant.class,id);
		f.setDepot(true);
	}

	@Override
	public Etudiant AfficherEtudiant(int id) {
		Etudiant f= em.find(Etudiant.class,id);
		return (f);
	}

	@Override
	public void ModifierEtudiant(Etudiant h) {
		em.merge(h);
		
	}

	@Override
	public List<Etudiant> getAllEtudiant() {
        return em.createNamedQuery("Etudiant.findAll", Etudiant.class).getResultList();

	}
	@Override
	public List<Etudiant> getAllEtudiantNoDepot() {
        return em.createNamedQuery("Etudiant.findAllNoDepot", Etudiant.class).getResultList();

	}
	@Override
	public List<Etudiant> getAllEtudiantDepot() {
        return em.createNamedQuery("Etudiant.findAllDepot", Etudiant.class).getResultList();

	}

	@Override
	public void deleteEtudiant(Etudiant h) {
		if (!em.contains(h)) {
            h = em.merge(h);
        }

        em.remove(h);	
	}
	
	/* ----------------------------------- Classe -------------------------------------------------- */ 

	@Override
	public void AjouterClasse (Classe  h) {
		em.persist(h);		
	}

	@Override
	public Classe  AfficherClasse (int id) {
		Classe  f= em.find(Classe .class,id);
		return (f);
	}

	@Override
	public void ModifierClasse (Classe  h) {
		em.merge(h);
		
	}

	@Override
	public List<Classe> getAllClasses() {
        return em.createNamedQuery("Classe .findAll", Classe .class).getResultList();

	}

	@Override
	public void deleteClasse (Classe  h) {
		if (!em.contains(h)) {
            h = em.merge(h);
        }

        em.remove(h);	
	}
	
	/* ----------------------------------- Site -------------------------------------------------- */ 
	
	@Override
	 public void AjouterSite (Site h)
	 {
		 em.persist(h);
	 }
	@Override
	 public Site AfficherSite (int id)
	 {
		Site s= em.find(Site.class,id);
		return (s);
		 
	 }
	@Override
	 public void ModifierSite (Site h)
	 {
		 em.merge(h);
		 
	 }
	@Override
	 public List<Site> getAllSites() {
		return em.createNamedQuery("Site.findAll", Site.class).getResultList();
	    }
	@Override
	 public void deleteSite(Site h) {
	        if (!em.contains(h)) {
	            h = em.merge(h);
	        }

	        em.remove(h);
	    }
	
	
	/* ----------------------------------- Departments -------------------------------------------------- */ 
	
	
	@Override
	public void AjouterDepartement(Departement h) {
		 em.persist(h);		
	}

	@Override
	public Departement AfficherDepartement(int id) {
		Departement s= em.find(Departement.class,id);
		return (s);
	}

	@Override
	public void ModifierDepartement(Departement h) {
		 em.merge(h);
		
	}

	@Override
	public List<Departement> getAllDepartement() {
		 return em.createNamedQuery("Departement.findAll", Departement.class).getResultList();
	}

	@Override
	public void deleteDepartement(Departement h) {
		 if (!em.contains(h)) {
	            h = em.merge(h);
	        }

	        em.remove(h);
	}
	
	/* ----------------------------------- Options -------------------------------------------------- */ 
	
	@Override
	public void AjouterOptions(Options h) {
		 em.persist(h);		
	}

	@Override
	public Options AfficherOptions(int id) {
		Options s= em.find(Options.class,id);
		return (s);
	}

	@Override
	public void ModifierOptions(Options h) {
		 em.merge(h);
		
	}

	@Override
	public List<Options> getAllOptions() {
		 return em.createNamedQuery("Options.findAll", Options.class).getResultList();
	}

	@Override
	public void deleteOptions(Options h) {
		 if (!em.contains(h)) {
	            h = em.merge(h);
	        }

	        em.remove(h);	
	}
	
	@Override
	public void Affecterdepasit(int ids, int idd) {
		Site s=em.find(Site.class, ids);
		Departement d=em.find(Departement.class, idd);
		d.setSite(s);
		
	}

	@Override
	public void Affecteropadep(int idd, int ido) {
		Departement d=em.find(Departement.class, idd);
		Options s=em.find(Options.class, ido);
		s.setDepartement(d);		
	}

	@Override
	public void Affecterclassaop(int ido, int idc) {
		Options s=em.find(Options.class, ido);
		Classe d=em.find(Classe.class, idc);
		d.setOption(s);		
	}
	
	public void Affecteretuaclass(int idc ,int ide){
		Classe c=em.find(Classe.class, idc);
		Etudiant e=em.find(Etudiant.class, ide);
		e.setClasse(c);	
		
	}
	

	
	/* ----------------------------------- Enseigants -------------------------------------------------- */ 
	@Override
	public void AjouterEnseignant(Enseignant E) {
		em.persist(E);
		
	}
	@Override
	public List<Enseignant> getAllEnseignant() {
        //return em.createNamedQuery("FormulaireConvention.findAll", FormulaireConvention.class).getResultList();
		return em.createNamedQuery("Enseignant.findAll",Enseignant.class).getResultList();

	}
	           /* --------------------------- Rapporteur ----------------------------------- */
	@Override
	public void AjouterRapporteur(Rapporteur R) {
		em.persist(R);
		
	}
	@Override
	public void FixNRapporteur(int rid,int nR) {
		Rapporteur rapporteur = em.find(Rapporteur.class, rid);
		rapporteur.setNmaxrapporter(nR);
	}
	@Override
	public List<Rapporteur> getAllRapporteur() {
        //return em.createNamedQuery("FormulaireConvention.findAll", FormulaireConvention.class).getResultList();
		return em.createNamedQuery("Rapporteur.findAll",Rapporteur.class).getResultList();

	}
	@Override
	public List getAllRapporteurBy(String title) {
		String jpql = "Select NEW tn.esprit.PIPFE.Entities.Rapporteur (t.Id,t.Cin,t.Nom,t.Prenom,t.Tel,t.Email,t.nmaxrapporter,t.nrapporter) FROM Rapporteur t WHERE t.Nom  like :param or t.Prenom  like :param ";
		javax.persistence.Query query =   em.createQuery(jpql);
		query.setParameter("param", "%"+title+"%");
		return query.getResultList();
		
		
	}
	
	/* --------------------------- Encadrant ----------------------------------- */
	@Override
	public void AjouterEncadrant(Encadrant E) {
		em.persist(E);
		
	}
	@Override
	public void FixNEncadrant(int rid,int nR) {
		Encadrant encadrant = em.find(Encadrant.class, rid);
		encadrant.setNmaxEncadrer(nR);
	}
	@Override
	public List<Encadrant> getAllEncadrant() {
        //return em.createNamedQuery("FormulaireConvention.findAll", FormulaireConvention.class).getResultList();
		return em.createNamedQuery("Encadrant.findAll",Encadrant.class).getResultList();

	}
	@Override
	public List getAllEncadrantBy(String title) {
		String jpql = "SELECT NEW tn.esprit.PIPFE.Entities.Encadrant (t.Id,t.Cin,t.Nom,t.Prenom,t.Tel,t.Email,t.nmaxEncadrer,t.nEncadrer) FROM Encadrant t WHERE t.Nom  like :param or t.Prenom  like :param ";
		javax.persistence.Query query =   em.createQuery(jpql);
		query.setParameter("param", "%"+title+"%");
		return query.getResultList();
	}
	/* --------------------------- Pre-validateur ----------------------------------- */
	@Override
	public void FixNPrevalider(int rid,int nR) {
		PreValidateur pre = em.find(PreValidateur.class, rid);
		pre.setNmaxValider(nR);
	}
	@Override
	public void AjouterPreValidateur(PreValidateur P) {
		em.persist(P);
		
	}
	@Override
	public List<PreValidateur> getAllPrevalidateur() {
        //return em.createNamedQuery("FormulaireConvention.findAll", FormulaireConvention.class).getResultList();
		return em.createNamedQuery("PreValidateur.findAll",PreValidateur.class).getResultList();

	}
	@Override
	public List getAllPrevalidateurBy(String title) {
		String jpql = "SELECT NEW tn.esprit.PIPFE.Entities.PreValidateur (t.Id,t.Cin,t.Nom,t.Prenom,t.Tel,t.Email,t.nmaxValider,t.nValider) FROM PreValidateur t WHERE t.Nom  like :param or t.Prenom  like :param ";
		javax.persistence.Query query =   em.createQuery(jpql);
		query.setParameter("param", "%"+title+"%");
		return query.getResultList();
	}
	/* --------------------------- President ----------------------------------- */
	@Override
	public void AffecterPresiSout(int p,int sout) {
		President a= em.find(President.class,p);
		Soutenance b= em.find(Soutenance.class,sout);
b.getFichePfe().getEnseignant().add(a);
a.increment();
b.setHavepresident(true);

			//a.getFichePFE().add(b);
		//b.getEnseignant().add(a);
		}
		
	@Override
	public Soutenance getSoutenanceById(int P) {
		return em.find(Soutenance.class,P);
	}
	@Override
	public void AjouterPresident(President P) {
		em.persist(P);
	}
	@Override
	public void FixNSoutener(int rid,int nR) {
		President pre = em.find(President.class, rid);
		pre.setNmaxSoutener(nR);
	}
	@Override
	public List<President> getAllPresident() {
        //return em.createNamedQuery("FormulaireConvention.findAll", FormulaireConvention.class).getResultList();
		return em.createNamedQuery("President.findAll",President.class).getResultList();

	}
	@Override
	public List getAllPresidentBy(String title) {
		String jpql = "SELECT NEW tn.esprit.PIPFE.Entities.President (t.Id,t.Cin,t.Nom,t.Prenom,t.Tel,t.Email,t.nmaxSoutener,t.nSoutener) FROM President t WHERE t.Nom  like :param or t.Prenom  like :param ";
		javax.persistence.Query query =   em.createQuery(jpql);
		query.setParameter("param", "%"+title+"%");
		return query.getResultList();
	}
	
	
	@Override
	public void AffecterCatEncadrant(int idE, int idC) {
		Encadrant ens=em.find(Encadrant.class, idE);
		Categorie cat=em.find(Categorie.class, idC);
		
		ens.getPrefEncadrerrList().add(cat);
		cat.getEncadrants().add(ens);	
	}
	
	@Override
	public void AffecterCatPrevalidateur(int idE, int idC) {
		PreValidateur ens=em.find(PreValidateur.class, idE);
		Categorie cat=em.find(Categorie.class, idC);
		ens.getPrefValiderList().add(cat);
		cat.getPrevalidateurs().add(ens);	
		
	}
	@Override
	public void AffecterEnsAFiche(int idf, int ide) {
		Enseignant cat=em.find(Enseignant.class, ide);
		FichePFE ens=em.find(FichePFE.class, idf);
		
		cat.getFichePFE().add(ens);
		ens.getEnseignant().add(cat);
	}
	
	@Override
	public void AffecterRoleToRapporteur(int Ide) {
		Enseignant e=em.find(Enseignant.class, Ide);
		Rapporteur R= new Rapporteur(e.getCin());
		em.persist(R);
	}
	@Override
	public void AffecterRoleToEncadrant(int Ide) {
		Enseignant e=em.find(Enseignant.class, Ide);
		Encadrant R= new Encadrant(e.getCin());
		em.persist(R);
		
	}
	@Override
	public void AffecterRoleToPreValidateur(int Ide) {
		Enseignant e=em.find(Enseignant.class, Ide);
		PreValidateur R= new PreValidateur(e.getCin());
		em.persist(R);
	}
	@Override
	public void AffecterRoleToPresident(int Ide) {
		Enseignant e=em.find(Enseignant.class, Ide);
		President R= new President(e.getCin());
		em.persist(R);
		
	}
	
	
	/* ----------------------------------- Head of Departments -------------------------------------------------- */ 
	
	/* ----------------------------------- Categories -------------------------------------------------- */ 
	@Override
	public void AjouterCategorie(Categorie c) {
		em.persist(c);	
	}
	@Override
	public void ModifierCategorie(int idc, String nom) {
		Categorie e=em.find(Categorie.class, idc);
		e.setNom(nom);		
	}
	@Override
	public void DeleteCategorie(int idc) {
		Categorie e=em.find(Categorie.class, idc);
		em.remove(e);
	}
	@Override
	public Categorie AfficherCategorie(int id) {
		Categorie f= em.find(Categorie.class,id);
		return (f);
	}
	
	/* ----------------------------------- Directeur de Stages -------------------------------------------------- */
	@Override
	public void AjouterFichePfe(FichePFE f) {
		em.persist(f);
		
	}
	@Override
	public void AffecterEtudiantFiche(int idE, int idF) {
		Etudiant e=em.find(Etudiant.class, idE);
		FichePFE fiche=em.find(FichePFE.class, idF);
		
		fiche.setEtudiant(e);
		e.setFichePfe(fiche);
	}
	@Override
	public void Rating(int idd) {
		DirecteurDeStage DS=em.find(DirecteurDeStage.class, idd);
       
    	   if(DS.isEnableRate())
    		   DS.setEnableRate(false);
    	   else
    		   DS.setEnableRate(true);
		
	}
	@Override
	public DirecteurDeStage getDirecteur(int idd) {
		DirecteurDeStage e=em.find(DirecteurDeStage.class, idd);
		return e;
	}
	
	
	
	
	
	
	
	
	
	/* ----------------------------------- Ecole -------------------------------------------------- */ 
	
	/* ----------------------------------- Fiche Pfe -------------------------------------------------- */ 
	
	public FichePFE getFichePfeById(int id) {
        return em.find(FichePFE.class, id);
    }
	
	@Override
	public List<FichePFE> getAllFiche() {
	        return em.createNamedQuery("FichePFE.findAll", FichePFE.class).getResultList();
	    }
	@Override
	public List<FichePFE> getAllFicheSansNote() {
	        return em.createNamedQuery("FichePFE.findNoNote", FichePFE.class).getResultList();
	    }
	@Override
	public void RappelResp(int idf,String titre) {
		
		FichePFE f=em.find(FichePFE.class, idf);
		List<Enseignant> ens=f.getEnseignant();
		Rapporteur r = null;
		Encadrant e = null;
		for (Enseignant enseignant : ens) {
			String type=enseignant.getType_employe();
			if(type.matches("Rapporteur")){
				r=em.find(Rapporteur.class, enseignant.getId());
			}
			if(type.matches("Encadrant")){
				e=em.find(Encadrant.class, enseignant.getId());
			}
		}
		if(f.getNoteRapporteur()==0)
			{String mail="Bonjour ;\n"
					+ "La direction de stage vous rappelle de bien  evaluer le rapport de stage de la fiche ayant comme titre'"+titre+" dans les plus brefs délais.\n"
							+ "Cordialement" ;

		    String objet="Rappel";
		    String To=null;
			To=r.getEmail();
			SendingMail sm=new SendingMail(mail, To , objet);
			sm.envoyer();
	    }
		if(f.getNoteEncadrant()==0)
		{String mail="Bonjour ;\n"
				+ "La direction de stage vous rappelle de bien  noter la fiche PFE ayant comme titre'"+titre+"en tant qu'encadrant dans les plus brefs délais.\n"
				+ "Cordialement" ;
	    String objet="Rappel";
	    String To=null;
		To=e.getEmail();
		SendingMail sm=new SendingMail(mail, To , objet);
		sm.envoyer();
    }
		
	}
	
	/*@Override
	public ArrayList getffff() {
		List<Enseignant> lf =new ArrayList();
	        List<FichePFE> kk=(ArrayList<FichePFE>) em.createNamedQuery("FichePFE.findNoNote", FichePFE.class).getResultList();
	        for(FichePFE e:kk){
	    		
				FichePFE f=(FichePFE) em.find(FichePFE.class, e.getId());
				List<Enseignant> l8=f.getEnseignant();
				for (Enseignant enseignant : l8) {
					lf.add(enseignant);
						
					
					
				}
				//System.out.println(f);
				//l8.addAll(e.getEnseignant());
				//System.out.println(e.getEnseignant());
				
			}
	        return (ArrayList) lf;
	    }*/
	
	/* ----------------------------------- Soutenance -------------------------------------------------- */ 

	@Override
	public List<Soutenance> getAllSoutNoPres() {
        return em.createNamedQuery("Soutenance.findNoPres", Soutenance.class).getResultList();

	}
	@Override
	public boolean Estdispo(int idp) {
		President p=em.find(President.class, idp);
		if(p.getnSoutener()<p.getNmaxSoutener())
			return true;
		else 
			return false;
		
	}
	
	/* ----------------------------------- Entreprise -------------------------------------------------- */ 
	
	@Override
	public List<Entreprise> getAllEntreprise() {
        return em.createNamedQuery("Entreprise.findAll", Entreprise.class).getResultList();
        

	}
	@Override
	public void AjouterEntreprise(Entreprise e) {
        em.persist(e);
	}
	@Override
	public Entreprise getEntrepriseById(int id) {
		return em.find(Entreprise.class, id);
      
	}
	
	public void DoRateProcces(int ide,int s)
	{
		Entreprise entreprise=em.find(Entreprise.class, ide);
		
		float newscor=(entreprise.getScore()+s);
		float newnote=(newscor/(entreprise.getNbrrate()+1));
		
		entreprise.setScore(newscor);
		entreprise.incNbrrate();
		entreprise.setNote(newnote);
	}
	
/* ----------------------------------- Feedback -------------------------------------------------- */ 
	
	
	

}
