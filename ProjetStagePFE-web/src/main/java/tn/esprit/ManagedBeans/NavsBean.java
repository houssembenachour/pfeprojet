package tn.esprit.ManagedBeans;

import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

@ManagedBean
@ApplicationScoped
public class NavsBean {
  private String Opened;
  
  @PostConstruct
	public void Init(){
		Opened="Dashboard";
	}

public String getOpened() {
	return Opened;
}

public void setOpened(String opened) {
	Opened = opened;
}


  
  
}
