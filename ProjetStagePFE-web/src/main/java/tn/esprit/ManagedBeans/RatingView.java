package tn.esprit.ManagedBeans;


import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.persistence.criteria.CriteriaBuilder.In;

import org.primefaces.event.RateEvent;

import tn.esprit.PIPFE.Entities.Entreprise;



@ManagedBean(name = "ratingView") 
@ViewScoped
public class RatingView {
     
    private int rating1=0;   
    private int rating2=0;   
    private int rating3=0;   
    private int rating4=0;
    public boolean a=false;
    
    

   
   
    public boolean isA() {
		return a;
	}

	public void setA(boolean a) {
		this.a = a;
	}

	public void hello()
    {
    	int a=0;
    	a=a+rating1+rating2+rating3+rating4;
    	System.out.println(a);
    	
    }
 
    public Integer getRating1() {
        return rating1;
    }
 
    public void setRating1(Integer rating1) {
        this.rating1 = rating1;
    }
 
    public Integer getRating2() {
        return rating2;
    }
 
    public void setRating2(Integer rating2) {
        this.rating2 = rating2;
    }
 
    public Integer getRating3() {
        return rating3;
    }
 
    public void setRating3(Integer rating3) {
        this.rating3 = rating3;
    }
 
    public Integer getRating4() {
        return rating4;
    }
 
    public void setRating4(Integer rating4) {
    	
        this.rating4 = rating4;
    }
   
}
	

