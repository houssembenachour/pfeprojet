package tn.esprit.ManagedBeans;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

import tn.esprit.PIPFE.Entities.DirecteurDeStage;
import tn.esprit.PIPFE.Entities.Entreprise;
import tn.esprit.PIPFE.Entities.Etudiant;
import tn.esprit.PIPFE.Entities.FichePFE;
/*import tn.esprit.PIPFE.Entities.Encadrant;
import tn.esprit.PIPFE.Entities.PreValidateur;
import tn.esprit.PIPFE.Entities.President;
import tn.esprit.PIPFE.Entities.Rapporteur;*/
import tn.esprit.PIPFE.Entities.User;
import tn.esprit.PIPFE.Services.CrudServices;



@ManagedBean
@ViewScoped
public class UserBean {
	
	private String username;
	private String password;
	private String Role;
	private int id_employe;
	private User user;
	private int iduser;
	private Boolean loggedIn;
	
	//***************** houssem *****************//
	
	private DirecteurDeStage directeur;
	private int rating1;   
    private int rating2;   
    private int rating3;   
    private int rating4;
    private Etudiant etudiant;
    private Entreprise entreprise;
    private List<Entreprise> allentreprises; 
    private boolean ableToRate;
    
    
	/*private Encadrant Encadrant;
	private PreValidateur Prevalidateur;
	public Rapporteur Rapporteur;
	private President President;*/
	
	@EJB
	CrudServices cs;
	@PostConstruct
    void init()
    {
		 this.rating1=0;   
	      this.rating2=0;   
	      this.rating3=0;   
	      this.rating4=0;
    	DirecteurDeStage d=cs.getDirecteur(1);
    	System.out.println("***********Directeur**********");
    	System.out.println("recuperé");
    	System.out.println(d);
    	System.out.println("***********Directeur**********");
    	this.setDirecteur(d);
    	System.out.println("dans le bean");
    	System.out.println(directeur);
    	System.out.println("*********************");
    	
    	Etudiant e=cs.getEtudiantById(1);
    	System.out.println("***********Etudiant**********");
    	System.out.println("recuperé");
    	System.out.println(e);
    	System.out.println("***********Etudiant**********");
    	System.out.println("dans le bean");
    	this.setEtudiant(e);
    	System.out.println(etudiant);
    	System.out.println("*********************");
    	
    	FichePFE fiche=etudiant.getFichePfe();
    	System.out.println("***********fiche**********");
    	System.out.println(fiche);
    	
    	System.out.println("*********************");
    	
    	System.out.println(entreprise);
    	System.out.println("*********************");
    	System.out.println("valeur de enablerate avant ");
    	System.out.println(this.ableToRate);
    	if(fiche != null && !etudiant.isRated() && directeur.isEnableRate())
    	{
    		Entreprise entre=fiche.getEntreprise();
        	System.out.println("***********entreprise**********");
        	System.out.println("recuperé");
        	System.out.println(entre);
        	System.out.println("***********entreprise**********");
        	System.out.println("dans le bean");
        	this.setEntreprise(entre);
    			
    		this.setAbleToRate(true);
    	
    	}
    		
    	else 
    		this.setAbleToRate(false);
    	
    	System.out.println("*********************");
    	System.out.println("valeur de enablerate apres ");
    	System.out.println(this.ableToRate);
    	
    	
    }
	
	//***************** houssem *****************//
	
	
	public String Rate()
    {
		
		FacesMessage globalFacesMessage = new FacesMessage("your vote passed successfully !");
		FacesContext facesContext = FacesContext.getCurrentInstance();
		facesContext.addMessage(null, globalFacesMessage);
    	int a=0;
    	a=a+rating1+rating2+rating3+rating4;
    	
    	System.out.println(a);
    	String navigateTo ="null";
		cs.DoRateProcces(entreprise.getId(), a);
		
		System.out.println("*****rated etudiant avant********");
	     System.out.println(etudiant.isRated());
		cs.switchRateEtudiant(etudiant.getId());
		System.out.println("*****rated etudiant apres********");
	     System.out.println(etudiant.isRated());
	     this.ableToRate=false;
		navigateTo = "/pages/Enseigant/Rate?faces-redirect=true";
		return navigateTo; 
		
    	
    }
 
	public List<Entreprise> getAllentreprises() {
		allentreprises = cs.getAllEntreprise();
		return allentreprises;
	}

	public void setAllentreprises(List<Entreprise> allentreprises) {
		this.allentreprises = allentreprises;
	}

	public String switshEnable(){
		String navigateTo ="null";
		cs.Rating(directeur.getId());
		DirecteurDeStage d=cs.getDirecteur(1);
    	this.setDirecteur(d);
		navigateTo = "/pages/Enseigant/InternshipDirector?faces-redirect=true";
		return navigateTo; 
}

	
	
	
    public DirecteurDeStage getDirecteur() {
		return directeur;
	}
	public void setDirecteur(DirecteurDeStage directeur) {
		this.directeur = directeur;
	}
	public Etudiant getEtudiant() {
		return etudiant;
	}
	public void setEtudiant(Etudiant etudiant) {
		this.etudiant = etudiant;
	}
	public Entreprise getEntreprise() {
		return entreprise;
	}
	public void setEntreprise(Entreprise entreprise) {
		this.entreprise = entreprise;
	}
	
	
	public void setRating1(int rating1) {
		this.rating1 = rating1;
	}
	public void setRating2(int rating2) {
		this.rating2 = rating2;
	}
	public void setRating3(int rating3) {
		this.rating3 = rating3;
	}
	public void setRating4(int rating4) {
		this.rating4 = rating4;
	}
	
	
	public int getRating1() {
		return rating1;
	}

	public int getRating2() {
		return rating2;
	}

	public int getRating3() {
		return rating3;
	}

	public int getRating4() {
		return rating4;
	}

	public boolean isAbleToRate() {
		return ableToRate;
	}

	public void setAbleToRate(boolean ableToRate) {
		this.ableToRate = ableToRate;
	}

	
	
	
	//***************** houssem *****************//
	
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getRole() {
		return Role;
	}

	public void setRole(String role) {
		Role = role;
	}

	public int getId_employe() {
		return id_employe;
	}

	public void setId_employe(int id_employe) {
		this.id_employe = id_employe;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public int getIduser() {
		return iduser;
	}

	public void setIduser(int iduser) {
		this.iduser = iduser;
	}

	public Boolean getLoggedIn() {
		return loggedIn;
	}

	public void setLoggedIn(Boolean loggedIn) {
		this.loggedIn = loggedIn;
	}
	
	/*public Encadrant getEncadrant() {
		Encadrant = cs.getEncadrantQuery(user);
		return Encadrant;
	}

	public void setEncadrant(Encadrant encadrant) {
		Encadrant = encadrant;
	}

	public PreValidateur getPrevalidateur() {
		Prevalidateur = cs.getPrevalidateurQuery(user);
		return Prevalidateur;
		
	}

	public void setPrevalidateur(PreValidateur prevalidateur) {
		Prevalidateur = prevalidateur;
	}

	public void setRapporteur(Rapporteur rapporteur) {
		Rapporteur = rapporteur;
	}
	
	public Rapporteur getRapporteur(){
		Rapporteur =cs.getRapporteurQuery(user);
		return Rapporteur;
	}
	

	public void setPresident(President president) {
		President = president;
	}
	
	public President getPresident(){
		
		President = cs.getPresidentQuery(user);
		return President;
	}*/

	public String doLogin(){
		String navigateTo="";
		user =cs.login(username, password);
		if (user != null){
		Role=user.getRole();
		id_employe=user.getId_employe();
		}
		
		if (user != null && user.getRole().equals("Enseignant")){
			navigateTo="/pages/Enseignant/Enseignant?faces-redirect=true";
			loggedIn=true;
		}
		else if (user != null && user.getRole().equals("")){
			navigateTo="";
			loggedIn=true;
		}
		else if (user != null && user.getRole().equals("")){
			navigateTo="";
			loggedIn=true;
		}
		else {
			FacesContext.getCurrentInstance().addMessage("form:btn", new FacesMessage("bad credentials"));
		}
		
		
		return navigateTo;
	}
	
	public String doLogout(){
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
		return "";
	}
	

	
	
	
	
	
	
}
